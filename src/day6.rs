use crate::util::{Result, read_lines};

pub fn pt1(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;
    let signal = &lines[0];

    let mut i1 = signal.chars();
    let mut i2 = signal.chars();
    let mut i3 = signal.chars();
    let mut i4 = signal.chars().enumerate();

    i2.nth(0);
    i3.nth(1);
    i4.nth(2);

    for (i, c4) in i4 {
        let c1 = i1.next().unwrap();
        let c2 = i2.next().unwrap();
        let c3 = i3.next().unwrap();

        if c1 != c2 && c1 != c3 && c1 != c4 && c2 != c3 && c2 != c4 && c3 != c4 {
            return Ok((i + 1) as u64);
        }
    }

    Ok(0)
}

pub fn pt2(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;
    let signal = &lines[0];

    let mut buffer: [u8; 26] = [0; 26];
    let mut dropper = signal.as_bytes().iter();

    for (i, c) in signal.as_bytes().iter().enumerate() {
        let idx = (c - b'a') as usize;
        buffer[idx] += 1;
        if i > 13 {
            let d = dropper.next().unwrap();
            buffer[(d - b'a') as usize] -= 1;
        }

        println!("{idx} - {buffer:?}");

        if check_unique(&buffer) && i > 13 {
            return Ok((i + 1) as u64);
        }
    }

    Ok(0)
}

fn check_unique(b: &[u8; 26]) -> bool {
    b.iter().max().unwrap() == &1
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day6pt1_demo() {
        assert_eq!(7, pt1("./data/day6_demo").unwrap());
    }

    #[test]
    fn test_day6pt1() {
        assert_eq!(1876, pt1("./data/day6").unwrap());
    }

    #[test]
    fn test_day6pt2_demo() {
        assert_eq!(19, pt2("./data/day6_demo").unwrap());
    }

    #[test]
    fn test_day6pt2() {
        assert_eq!(19, pt2("./data/day6").unwrap());
    }
}
