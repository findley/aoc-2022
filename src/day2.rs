use std::str::FromStr;

use crate::util::{read_lines, Result, AocError};

pub fn pt1(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;

    let score = lines.iter().fold(0, |a, line| {
        let mut words = line.split_whitespace().take(2);
        let opponent_move = words.next().map(|w| w.parse::<Rps>().unwrap()).unwrap();
        let my_move = words.next().map(|w| w.parse::<Rps>().unwrap()).unwrap();

        let outcome = match (&my_move, opponent_move) {
            (Rps::Rock, Rps::Scissors) | (Rps::Paper, Rps::Rock) | (Rps::Scissors, Rps::Paper) => Outcome::Win,
            (s, o) if *s == o => Outcome::Draw,
            _ => Outcome::Lose,
        };

        a + outcome.points() + my_move.points()
    });

    Ok(score)
}

pub fn pt2(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;

    let score = lines.iter().fold(0, |a, line| {
        let mut words = line.split_whitespace().take(2);
        let opponent_move = words.next().map(|w| w.parse::<Rps>().unwrap()).unwrap();
        let outcome = words.next().map(|w| w.parse::<Outcome>().unwrap()).unwrap();

        let my_move = match (&outcome, &opponent_move) {
            (Outcome::Draw, _) => opponent_move,
            (Outcome::Win, Rps::Rock) => Rps::Paper,
            (Outcome::Win, Rps::Paper) => Rps::Scissors,
            (Outcome::Win, Rps::Scissors) => Rps::Rock,
            (Outcome::Lose, Rps::Rock) => Rps::Scissors,
            (Outcome::Lose, Rps::Paper) => Rps::Rock,
            (Outcome::Lose, Rps::Scissors) => Rps::Paper,
        };

        a + outcome.points() + my_move.points()
    });

    Ok(score)
}

#[derive(PartialEq, Debug, Eq)]
enum Rps {
    Rock,
    Paper,
    Scissors,
}

#[derive(PartialEq, Debug, Eq)]
enum Outcome {
    Win,
    Draw,
    Lose,
}

impl Rps {
    fn points(&self) -> u64 {
        match self {
            Self::Rock => 1, Self::Paper => 2, Self::Scissors => 3,
        }
    }
}

impl Outcome {
    fn points(&self) -> u64 {
        match self {
            Self::Win => 6, Self::Draw => 3, Self::Lose => 0,
        }
    }
}

impl FromStr for Rps {
    type Err = AocError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "A" | "X" => Ok(Self::Rock),
            "B" | "Y" => Ok(Self::Paper),
            "C" | "Z" => Ok(Self::Scissors),
            _ => Err(AocError),
        }
    }
}

impl FromStr for Outcome {
    type Err = AocError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "X" => Ok(Self::Lose),
            "Y" => Ok(Self::Draw),
            "Z" => Ok(Self::Win),
            _ => Err(AocError),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day2pt1_demo() {
        assert_eq!(15, pt1("./data/day2_demo").unwrap());
    }

    #[test]
    fn test_day2pt1() {
        assert_eq!(14827, pt1("./data/day2").unwrap());
    }

    #[test]
    fn test_day2pt2_demo() {
        assert_eq!(12, pt2("./data/day2_demo").unwrap());
    }

    #[test]
    fn test_day2pt2() {
        assert_eq!(13889, pt2("./data/day2").unwrap());
    }
}
