use std::{collections::VecDeque, fmt::Display};

use crate::util::{Result, read_lines};

struct Stacks(Vec<VecDeque<char>>);

pub fn pt1(filename: &'static str) -> Result<String> {
    let lines = read_lines(filename)?;

    let mut sections = lines.split(|l| l.starts_with(" 1"));
    let stack_lines = sections.next().expect("Bad Input");
    let instruction_lines = &sections.next().expect("Bad Input")[1..];

    let mut stack = Stacks::from_lines(stack_lines);
    for instruction in instruction_lines.iter() {
        stack.run_instruction(instruction)?;
    }

    Ok(stack.read_top())
}

pub fn pt2(filename: &'static str) -> Result<String> {
    let lines = read_lines(filename)?;

    let mut sections = lines.split(|l| l.starts_with(" 1"));
    let stack_lines = sections.next().expect("Bad Input");
    let instruction_lines = &sections.next().expect("Bad Input")[1..];

    let mut stack = Stacks::from_lines(stack_lines);
    for instruction in instruction_lines.iter() {
        stack.run_instruction_9001(instruction)?;
    }

    Ok(stack.read_top())
}

impl Stacks {
    pub fn from_lines(lines: &[String]) -> Self {
        let stack_count: usize = (lines[0].len() as f64 / 4.0).ceil() as usize;
        let mut stacks = vec![VecDeque::new(); stack_count];

        for line in lines.iter() {
            for i in 0..stack_count {
                let c = line.chars().nth(i*4+1).expect("Bad input");
                if c != ' ' {
                    stacks[i].push_front(c);
                }
            }
        }


        Stacks(stacks)
    }

    pub fn run_instruction(&mut self, instruction: &String) -> Result<()> {
        let mut parts = instruction.split_whitespace();
        parts.next(); // skip move
        let amount = parts.next().expect("Bad input").parse::<usize>()?;
        parts.next(); // skip from
        let src = parts.next().expect("Bad input").parse::<usize>()?;
        parts.next(); // skip to
        let dst = parts.next().expect("Bad input").parse::<usize>()?;

        for _ in 0..amount {
            let item = self.0[src-1].pop_back();
            if let Some(c) = item {
                self.0[dst-1].push_back(c);
            }
        }

        Ok(())
    }

    pub fn run_instruction_9001(&mut self, instruction: &String) -> Result<()> {
        let mut parts = instruction.split_whitespace();
        parts.next(); // skip move
        let amount = parts.next().expect("Bad input").parse::<usize>()?;
        parts.next(); // skip from
        let src = parts.next().expect("Bad input").parse::<usize>()?;
        parts.next(); // skip to
        let dst = parts.next().expect("Bad input").parse::<usize>()?;

        let parts: Vec<_> = self.0[src-1].iter().rev().take(amount).rev().copied().collect();

        for c in parts.into_iter() {
            self.0[dst-1].push_back(c);
            self.0[src-1].pop_back();
        }

        Ok(())
    }

    pub fn read_top(&self) -> String {
        self.0.iter().map(|s| s.back().unwrap_or(&' ')).collect()
    }
}

impl Display for Stacks {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let height = self.0.iter().map(|s| s.len()).max().expect("wut?");
        for h in (0..height).rev() {
            for c in self.0.iter().map(|s| s.get(h).unwrap_or(&' ')) {
                write!(f, "[{}] ", &c)?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day5pt1_demo() {
        assert_eq!("CMZ", pt1("./data/day5_demo").unwrap());
    }

    #[test]
    fn test_day5pt1() {
        assert_eq!("DHBJQJCCW", pt1("./data/day5").unwrap());
    }

    #[test]
    fn test_day5pt2_demo() {
        assert_eq!("MCD", pt2("./data/day5_demo").unwrap());
    }

    #[test]
    fn test_day5pt2() {
        assert_eq!("WJVRLSJJT", pt2("./data/day5").unwrap());
    }
}
