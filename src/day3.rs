use crate::util::{Result, read_lines};

pub fn pt1(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;

    let mut priority_sum = 0;

    for line in lines.iter() {
        let left_items = bag_to_inventory(&line[..line.len()/2].to_string());
        let right_items = bag_to_inventory(&line[line.len()/2..].to_string());

        for (i, (l, r)) in left_items.iter().zip(right_items).enumerate() {
            if *l && r {
                priority_sum += i+1;
            }
        }
    }

    Ok(priority_sum as u64)
}

pub fn pt2(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;

    let mut priority_sum = 0;

    for group in lines.chunks(3) {
        let b1 = bag_to_inventory(&group[0]);
        let b2 = bag_to_inventory(&group[1]);
        let b3 = bag_to_inventory(&group[2]);
        for i in 0..52 {
            if b1[i] && b2[i] && b3[i] {
                priority_sum += i+1;
            }
        }
    }

    Ok(priority_sum as u64)
}

fn bag_to_inventory(bag: &String) -> [bool; 52] {
    let mut inventory = [false; 52];
    for c in bag.bytes() {
        let item = char_to_num(c);
        inventory[item-1] = true;
    }

    return inventory;
}

fn char_to_num(c: u8) -> usize {
    match c {
        v if v >= 97 && v <= 122 => (v - 96).into(),
        v if v >= 65 && v <= 90 => (v - 38).into(),
        _ => panic!("Unexpected item"),
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day3pt1_demo() {
        assert_eq!(157, pt1("./data/day3_demo").unwrap());
    }

    #[test]
    fn test_day1pt1() {
        assert_eq!(7766, pt1("./data/day3").unwrap());
    }

    #[test]
    fn test_day3pt2_demo() {
        assert_eq!(70, pt2("./data/day3_demo").unwrap());
    }

    #[test]
    fn test_day3pt2() {
        assert_eq!(2415, pt2("./data/day3").unwrap());
    }
}
