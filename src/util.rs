use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::{BufRead, BufReader};

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(Debug)]
pub struct AocError;

impl AocError {
    pub fn new() -> Box<Self> {
        Box::new(AocError{})
    }
}

impl Error for AocError {}

impl fmt::Display for AocError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Oops!")
    }
}

pub fn read_lines(filename: &'static str) -> Result<Vec<String>> {
    let file = File::open(filename)?;
    let reader = BufReader::new(file);

    Ok(reader
        .lines()
        .map(|line_result| line_result.unwrap())
        .collect())
}
