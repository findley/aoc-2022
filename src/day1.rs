use std::collections::BinaryHeap;
use crate::util::{Result, read_lines};

pub fn pt1(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;

    let mut current = 0;
    let mut max = 0;
    for line in lines {
        if line.is_empty() {
            if current > max {
                max = current;
            }
            current = 0;
        } else {
            current += line.parse::<u64>().unwrap();
        }
    }
    if current > max {
        max = current;
    }

    Ok(max)
}

pub fn pt2(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;

    let mut heap = BinaryHeap::<u64>::new();
    let mut current = 0;
    for line in lines {
        if line.is_empty() {
            heap.push(current);
            if heap.len() > 3 {
                heap.shrink_to(3);
            }
            current = 0;
        } else {
            current += line.parse::<u64>()?;
        }
    }
    heap.push(current);

    Ok(heap.pop().unwrap() + heap.pop().unwrap() + heap.pop().unwrap())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day1pt1_demo() {
        assert_eq!(24000, pt1("./data/day1_demo").unwrap());
    }

    #[test]
    fn test_day1pt1() {
        assert_eq!(68442, pt1("./data/day1").unwrap());
    }

    #[test]
    fn test_day1pt2_demo() {
        assert_eq!(45000, pt2("./data/day1_demo").unwrap());
    }

    #[test]
    fn test_day1pt2() {
        assert_eq!(204837, pt2("./data/day1").unwrap());
    }
}
