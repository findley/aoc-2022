use std::ops::RangeInclusive;

use crate::util::{read_lines, Result};

pub fn pt1(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;

    let mut result = 0;

    for line in lines.iter() {
        let mut parts = line.split(',');
        let r1 = parse_range(parts.next().unwrap());
        let r2 = parse_range(parts.next().unwrap());
        if full_overlap(r1, r2) {
            result += 1;
        }
    }

    Ok(result)
}

pub fn pt2(filename: &'static str) -> Result<u64> {
    let lines = read_lines(filename)?;

    let mut result = 0;

    for line in lines.iter() {
        let mut parts = line.split(',');
        let r1 = parse_range(parts.next().unwrap());
        let r2 = parse_range(parts.next().unwrap());
        if partial_overlap(&r1, &r2) {
            result += 1;
        }
    }

    Ok(result)
}

fn full_overlap(r1: RangeInclusive<u64>, r2: RangeInclusive<u64>) -> bool {
    (r1.contains(r2.start()) && r1.contains(r2.end()))
        || (r2.contains(r1.start()) && r2.contains(r1.end()))
}

fn partial_overlap(r1: &RangeInclusive<u64>, r2: &RangeInclusive<u64>) -> bool {
    r1.start() <= r2.end() && r1.end() >= r2.start()
}

fn parse_range(s: &str) -> RangeInclusive<u64> {
    let mut parts = s.split('-');
    let start = parts.next().unwrap().parse::<u64>().unwrap();
    let end = parts.next().unwrap().parse::<u64>().unwrap();

    start..=end
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_day4pt1_demo() {
        assert_eq!(2, pt1("./data/day4_demo").unwrap());
    }

    #[test]
    fn test_day4pt1() {
        assert_eq!(569, pt1("./data/day4").unwrap());
    }

    #[test]
    fn test_day4pt2_demo() {
        assert_eq!(4, pt2("./data/day4_demo").unwrap());
    }

    #[test]
    fn test_day4pt2() {
        assert_eq!(936, pt2("./data/day4").unwrap());
    }
}
